# systemctl-edit-sudoedit
Using `systemctl edit`, kinda like you'd be using sudoedit!

## Usage
Simply replace all usage of `systemctl edit` with `systemctl-edit`.

Example:
```
systemctl-edit nginx
```
